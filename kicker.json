{
  "name": "Gradle",
  "description": "Build, test and analyse your [Gradle](https://gradle.org/)-based projects",
  "template_path": "templates/gitlab-ci-gradle.yml",
  "kind": "build",
  "prefix": "gradle",
  "is_component": true,
  "variables": [
    {
      "name": "GRADLE_IMAGE",
      "description": "The Docker image used to run Gradle - **set the version required by your project**",
      "default": "registry.hub.docker.com/library/gradle:latest"
    },
    {
      "name": "GRADLE_CLI_OPTS",
      "description": "Additional Gradle options used on the command line",
      "advanced": true
    },
    {
      "name": "GRADLE_CLI_BIN",
      "description": "The location of the gradle binary. If you prefer using a [gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) you should override this (for e.g. `gradlew`)",
      "default": "gradle",
      "advanced": true
    },
    {
      "name": "GRADLE_USER_HOME",
      "description": "The gradle user home",
      "default": "$CI_PROJECT_DIR/.gradle",
      "advanced": true
    },
    {
      "name": "GRADLE_DAEMON",
      "description": "Whether to use or not gradle daemon",
      "default": "false",
      "advanced": true
    },
    {
      "name": "GRADLE_BUILD_ARGS",
      "description": "Gradle arguments for the build & test job",
      "default": "build",
      "advanced": true
    },
    {
      "name": "GRADLE_PROJECT_DIR",
      "description": "Gradle project root directory",
      "default": ".",
      "advanced": true
    },
    {
      "name": "JACOCO_CSV_REPORT",
      "description": "Name of code coverage report",
      "default": "jacocoTestReport.csv",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "sonar",
      "name": "SONAR",
      "description": "Code quality and security analysis with [SONARQube](https://www.sonarqube.org/)",
      "variables": [
        {
          "name": "SONAR_HOST_URL",
          "type": "url",
          "description": "SonarQube server url",
          "mandatory": true
        },
        {
          "name": "SONAR_TOKEN",
          "description": "SonarQube authentication token (see https://docs.sonarsource.com/sonarqube-server/latest/user-guide/managing-tokens/)",
          "secret": true
        },
        {
          "name": "SONAR_BASE_ARGS",
          "description": "SonarQube [analysis arguments](https://docs.sonarsource.com/sonarqube-server/latest/analyzing-source-code/analysis-parameters/)",
          "default": "sonar -Dsonar.links.homepage=${CI_PROJECT_URL} -Dsonar.links.ci=${CI_PROJECT_URL}/-/pipelines -Dsonar.links.issue=${CI_PROJECT_URL}/-/issues",
          "advanced": true
        },
        {
          "name": "SONAR_QUALITY_GATE_ENABLED",
          "description": "Enables SonarQube [Quality Gate](https://docs.sonarsource.com/sonarqube-server/latest/instance-administration/analysis-functions/quality-gates/) verification.\n\n_Uses `sonar.qualitygate.wait` parameter ([see doc](https://docs.sonarsource.com/sonarqube-server/latest/analyzing-source-code/ci-integration/overview/#quality-gate-fails))._",
          "type": "boolean"
        }
      ]
    },
    {
      "id": "dependency-check",
      "name": "Dependency Check",
      "description": "Runs a Gradle dependency check",
      "disable_with": "GRADLE_DEPENDENCY_CHECK_DISABLED",
      "variables": [
        {
          "name": "GRADLE_DEPENDENCY_CHECK_TASK",
          "description": "The dependency-check task that is invoked",
          "default": "dependencyCheckAnalyze",
          "advanced": true
        }
      ]
    },
    {
      "id": "sbom",
      "name": "Software Bill of Materials",
      "description": "This job generates a file listing all dependencies using [cyclonedx-gradle-plugin](https://github.com/CycloneDX/cyclonedx-gradle-plugin)",
      "disable_with": "GRADLE_SBOM_DISABLED",
      "variables": [
        {
          "name": "TBC_SBOM_MODE",
          "type": "enum",
          "values": ["onrelease", "always"],
          "description": "Controls when SBOM reports are generated (`onrelease`: only on `$INTEG_REF`, `$PROD_REF` and `$RELEASE_REF` pipelines; `always`: any pipeline)",
          "advanced": true,
          "default": "onrelease"
        },
        {
          "name": "GRADLE_SBOM_VERSION",
          "description": "Version of the `cyclonedx-gradle-plugin` used for SBOM analysis.\n\n_When unset, the latest version will be used_"
        },
        {
          "name": "GRADLE_MAVEN_PLUGIN_URL",
          "description": "Maven Repository that is used to download the `cyclonedx-gradle-plugin`. No trailing slash.",
          "default": "https://plugins.gradle.org/m2"
        }
      ]
    },
    {
      "id": "publish",
      "name": "Publish (snapshot & release)",
      "description": "Enable publishing to an artifacts repository",
      "disable_with": "GRADLE_NO_PUBLISH",
      "variables": [
        {
          "name": "GRADLE_PUBLISH_ARGS",
          "description": "The publish task that is invoked",
          "default": "publish",
          "advanced": true
        },
        {
          "name": "GRADLE_PUBLISH_VERSION",
          "description": "The value is propagated as gradle properties named `version`.\n\nIt should be used in your publish task",
          "default": "${CI_COMMIT_REF_SLUG}-SNAPSHOT",
          "advanced": true
        }
      ]
    }
  ]
}
